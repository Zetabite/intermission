package zetabite.intermission;

import net.fabricmc.api.ClientModInitializer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Intermission implements ClientModInitializer {
    public static final String MODID = "intermission";
    public static final String MOD_NAME = "Intermission";
    public static final Logger LOG = LogManager.getLogger(MOD_NAME);

    @Override
    public void onInitializeClient() {
        LOG.info("Loaded " + MOD_NAME);
    }
}