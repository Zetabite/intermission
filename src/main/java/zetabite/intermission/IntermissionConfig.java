package zetabite.intermission;

import com.google.gson.*;
import me.shedaniel.clothconfig2.api.ConfigBuilder;
import me.shedaniel.clothconfig2.api.ConfigCategory;
import me.shedaniel.clothconfig2.impl.builders.BooleanToggleBuilder;
import me.shedaniel.clothconfig2.impl.builders.EnumSelectorBuilder;
import me.shedaniel.clothconfig2.impl.builders.IntFieldBuilder;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.text.LiteralText;
import net.minecraft.text.TranslatableText;

import java.io.*;

public class IntermissionConfig {
    private static boolean OVERRIDE_MUSIC_DELAY = false;
    private static TimeUnits TIME_UNIT = TimeUnits.SECONDS;
    private static int MUSIC_DELAY = 0;

    static {
        IntermissionConfig.load();
    }

    private static void save() {
        File configFile = new File("config" + File.separator + "intermission.json");
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(configFile));
            JsonObject jsonObject = new JsonObject();
            JsonObject generalCategory = new JsonObject();

            generalCategory.addProperty("max_music_delay", MUSIC_DELAY);
            generalCategory.addProperty("override_music_delay", OVERRIDE_MUSIC_DELAY);
            generalCategory.addProperty("time_unit", TIME_UNIT.name());
            jsonObject.add("general", generalCategory);

            writer.write(gson.toJson(jsonObject));
            writer.close();
        } catch (IOException e) {
            Intermission.LOG.warn("Couldn't write configs to file!");
        }
    }

    private static void load() {
        try {
            InputStream inputStream = new FileInputStream("config" + File.separator + "intermission.json");
            JsonParser jsonParser = new JsonParser();

            JsonObject json = (JsonObject)jsonParser.parse(new InputStreamReader(inputStream));

            if (json.has("general")) {
                JsonObject generalCategory = json.getAsJsonObject("general");

                OVERRIDE_MUSIC_DELAY = generalCategory.get("override_music_delay").getAsBoolean();
                TIME_UNIT = TimeUnits.valueOf(generalCategory.get("time_unit").getAsString());
                MUSIC_DELAY = generalCategory.get("max_music_delay").getAsInt();
                MUSIC_DELAY *= (MUSIC_DELAY > 0 ? 1 : -1);
            }

            inputStream.close();
        } catch (FileNotFoundException e) {
            Intermission.LOG.warn("Config file couldn't be found, creating default...");
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IntermissionConfig.save();
        }
    }

    public static Screen makeScreen(Screen parent) {
        IntermissionConfig.load();

        ConfigBuilder builder = ConfigBuilder.create()
                .setParentScreen(parent)
                .setTitle(new LiteralText(Intermission.MOD_NAME));

        ConfigCategory generalCategory = builder.getOrCreateCategory(new TranslatableText("category.intermission.general"));

        generalCategory.addEntry(new BooleanToggleBuilder(
                new TranslatableText("text.cloth-config.reset_value"), new TranslatableText("intermission.config.value.override_music_delay"), OVERRIDE_MUSIC_DELAY)
                .setDefaultValue(false)
                .setSaveConsumer(b -> OVERRIDE_MUSIC_DELAY = b)
                .build()
        );

        generalCategory.addEntry(new EnumSelectorBuilder<TimeUnits>(
                new TranslatableText("text.cloth-config.reset_value"), new TranslatableText("intermission.config.value.time_unit"), (Class<TimeUnits>) TIME_UNIT.getClass(), TIME_UNIT)
                .setDefaultValue(TimeUnits.SECONDS)
                .setSaveConsumer(e -> TIME_UNIT = e)
                .build()
        );

        generalCategory.addEntry(new IntFieldBuilder(
                new TranslatableText("text.cloth-config.reset_value"), new TranslatableText("intermission.config.value.max_music_delay"), MUSIC_DELAY)
                .setDefaultValue(0)
                .setSaveConsumer(i -> MUSIC_DELAY = i)
                .build()
        );

        MUSIC_DELAY *= (MUSIC_DELAY > 0 ? 1 : -1);

        builder.setSavingRunnable(IntermissionConfig::save);

        return builder.build();
    }

    private enum TimeUnits {
        TICKS(1),
        SECONDS(20),
        MINUTES(20 * 60);

        private final int multiplier;
        TimeUnits(int multiplier) {
            this.multiplier = multiplier;
        }

        public int getMultiplier() {
            return this.multiplier;
        }
    }

    public static boolean overrideMusicDelay() {
        return OVERRIDE_MUSIC_DELAY;
    }

    public static int getMusicDelay() {
        return MUSIC_DELAY * TIME_UNIT.getMultiplier();
    }
}
