package zetabite.intermission.mixin;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.sound.MusicTracker;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import zetabite.intermission.IntermissionConfig;

@Environment(EnvType.CLIENT)
@Mixin(MusicTracker.class)
public class MusicTrackerMixin {
    @Shadow private int timeUntilNextSong;

    @Inject(method = "tick", at = @At("TAIL"))
    private void injectMaxDelayCheck(CallbackInfo info) {
        if(IntermissionConfig.overrideMusicDelay()) {
            this.timeUntilNextSong = Math.min(this.timeUntilNextSong, IntermissionConfig.getMusicDelay());
        }
    }
}
